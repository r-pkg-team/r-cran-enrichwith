Source: r-cran-enrichwith
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-enrichwith
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-enrichwith.git
Homepage: https://cran.r-project.org/package=enrichwith
Rules-Requires-Root: no

Package: r-cran-enrichwith
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R methods to enrich R Objects with extra components
 Provides the "enrich" method to enrich list-like R objects with new,
 relevant components. The current version has methods for enriching
 objects of class 'family', 'link-glm', 'lm', 'glm' and 'betareg'. The
 resulting objects preserve their class, so all methods associated with
 them still apply. The package also provides the 'enriched_glm' function
 that has the same interface as 'glm' but results in objects of class
 'enriched_glm'. In addition to the usual components in a `glm` object,
 'enriched_glm' objects carry an object-specific simulate method and
 functions to compute the scores, the observed and expected information
 matrix, the first-order bias, as well as model densities,
 probabilities, and quantiles at arbitrary parameter values. The package
 can also be used to produce customizable source code templates for the
 structured implementation of methods to compute new components and
 enrich arbitrary objects.
